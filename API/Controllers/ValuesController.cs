﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.SignalR.Client;
using System.Threading.Tasks;
using System.Windows;
//using Microsoft.AspNetCore.SignalR.Client;

namespace API.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public HttpResponseMessage Get()
        {

            WebRequest request = WebRequest.Create(
              "http://localhost:55409/api/signalapi");
            var r = request.GetResponse();

            return Request.CreateResponse(
                HttpStatusCode.OK,
                "Done!",
                Configuration.Formatters.JsonFormatter
            ) ;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
