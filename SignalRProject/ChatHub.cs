﻿using Microsoft.AspNetCore.SignalR;

namespace SignalRProject.Hubs
{
    public class ChatHub : Hub
    {
        private static IHubContext<ChatHub> _hubContext;

        public ChatHub(IHubContext<ChatHub> hubContext) {
            _hubContext = hubContext;
        }

        // Call this from JS: hub.client.send(channel, content)
        public void Send(string channel, string content)
        {
            _hubContext.Clients.All.SendAsync("Sendxz", channel, content);
        }

        // Call this from C#: NewsFeedHub.Static_Send(channel, content)
        public static void Static_Send(string channel, string content)
        {
            _hubContext.Clients.All.SendAsync("Sendxz", channel, content);
        }
    }
}
